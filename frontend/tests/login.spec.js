import { expect, test } from "@playwright/test";

test("login", () => {
  test("Email format not correct should come after user has written something", async ({
    page,
  }) => {
    const loginPage = await page.goto("http://localhost:5173/login");
    expect(loginPage).toContain("Email format not correct");
  });
});
